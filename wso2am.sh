#!/bin/bash

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# THIS IS ONLY A SAMPLE.  OBVIOUSLY YOU SHOULD NEVER INCLUDE PASSWORDS IN
# SOURCE CODE OR SCRIPTS TO BE STORED IN A VERSION CONTROL REPOSITORY.
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# *****************************************************************************
# PREFERRED APPROACH
# *****************************************************************************

# 
# Setup the secrets required by this stack.
# This should be done in advance by whoever manages credentials
#
docker secret inspect WSO2AM_MYSQL_PASSWORD > /dev/null
if (( $? != 0 )); then
    echo supersecretpassword | docker secret create WSO2AM_MYSQL_PASSWORD -
fi

docker secret inspect WSO2AM_MYSQL_ROOT_PASSWORD > /dev/null
if (( $? != 0 )); then
    echo superdupersecredpassword | docker secret create WSO2AM_MYSQL_ROOT_PASSWORD -
fi

if [[ ! -d /data/wso2db ]]; then
    mkdir -p /data/wso2db
fi

# 
# Launch Docker stack as defined in the Docker compose file specified.
# 
docker stack deploy --compose-file wso2am.yml WSO2AM

# *****************************************************************************
# OTHER EXAMPLES
# *****************************************************************************

# -----------------------------------------------------------------------------
# Launch Docker container on the current node.
# 
# This is a sample.  If you really think I would put a password in a production
# deployment script than you are too stupid to be looking at this example.
# -----------------------------------------------------------------------------
# docker run --name wso2db-live --restart=always --detach \
#                   --publish=3306:3306 \
#                   --volume=/data/wso2db:/var/lib/mysql \
#                   -e MYSQL_ROOT_PASSWORD=Zummer#69 \
#                   -e MYSQL_USER=apimgr \
#                   -e MYSQL_PASSWORD=Zummer#69 \
#                   salte/mysql-wso2am:2.0.0

# -----------------------------------------------------------------------------
# Create's a single instance of a Docker Swarm Service.  This may run on any
# node in the SWARM.
# 
# This is a sample.  If you really think I would put a password in a production
# deployment script than you are too stupid to be looking at this example.
# -----------------------------------------------------------------------------
# docker service create --replicas 1 --name wso2db --restart-condition any --detach \
#                       --publish 3306:3306 \
#                       --mount type=bind,source=/data/wso2db,destination=/var/lib/mysql \
#                       -e MYSQL_ROOT_PASSWORD=Zummer#69 \
#                       -e MYSQL_USER=apimgr \
#                       -e MYSQL_PASSWORD=Zummer#69 \
#                       salte/mysql-wso2am:2.0.0
